import React, { FC, useEffect, useState } from "react";
import "./History.scss";

// Redux
import { useDispatch, useSelector } from "react-redux";
import * as reduxActions from "Redux/Actions";
import { ReduxState, Payload } from "Redux/models/models";

// Utils
import { RouteComponentProps } from "react-router-dom";
import moment from "moment";

// Data
import data from "./data";

const History: FC<RouteComponentProps> = () => {
    const transactions = useSelector((state: ReduxState) => state.transactions);
    const dispatch = useDispatch();

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        setTimeout(() => {
            const theData: Payload[] = data;
            dispatch(reduxActions.getAllTransactions(theData));
            setIsLoading(false);
        }, 1000);
    }, []);

    console.log("state: ", transactions);
    return (
        <div id="history">
            <div className="content-header">Transaction History</div>
            <div className="content-main">
                <div className="content-main__filters"></div>
                <div className="content-main__table">
                    <div className="content-main__table__header">
                        <div className="content-main__table__header__date"><span>Date</span></div>
                        <div className="content-main__table__header__coin"><span>Coin</span></div>
                        <div className="content-main__table__header__amount"><span>Amount</span></div>
                        <div className="content-main__table__header__status"><span>Status</span></div>
                    </div>
                    <div className="content-main__table__content">
                        {transactions.map(transaction => {
                            return (
                                <div key={transaction.id} className="content-main__table__content__row">
                                    <div className="content-main__table__content__row__date"><span>{moment(transaction.payDate).format("YYYY-MM-DD, hh:mm:ss")}</span></div>
                                    <div className="content-main__table__content__row__coin"><span>{transaction.crypto.currency}</span></div>
                                    <div className="content-main__table__content__row__amount"><span>{transaction.crypto.amount}</span></div>
                                    <div className="content-main__table__content__row__status"><span>{transaction.state}</span></div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default History;