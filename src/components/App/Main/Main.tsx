import React from "react";
import "./Main.scss";

// components
import Header from "components/Header/Header";
import NavBar from "components/NavBar/NavBar";
import History from "components/Pages/History/History";
import Wallet from "components/Pages/Wallet/Wallet";


// Utils
import { Switch, Route, } from "react-router-dom";

function Main(): JSX.Element {
  return (
    <div className="page">
      <div id="header">
        <Header />
      </div>
      <div id="main">
        <div id="navbar">
          <NavBar />
        </div>
        <div id="content">
          <Switch>
            <Route exact path="/history" render={(props) => <History {...props} />} />
            <Route exact path="/wallet" render={(props) => <Wallet {...props} />} />
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default Main;