import React from "react";
import "./App.scss";

import Main from "components/App/Main/Main";

// Redux
import { Provider } from "react-redux";
import store from "Redux/Store";

// Utils
import { BrowserRouter as Router, } from "react-router-dom";

function App(): JSX.Element {
  return (
    <Router >
      <Provider store={store} >
        <Main />
      </Provider>
    </Router>
  );
}

export default App;
