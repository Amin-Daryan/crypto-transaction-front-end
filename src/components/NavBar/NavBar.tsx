import React, { useState } from "react";
import "./NavBar.scss";


// Utils
import { Link, } from "react-router-dom";
import { FaWallet, FaFileInvoice } from "react-icons/fa";

// export interface IAppProps {

// }

export default function NavBar(): JSX.Element {
  const [selectedTab, setSelectedTab] = useState(1);

  return (
    <div className="navbar">
      <ul className="navbar__list">
        <li className={`navbar__list__item ${selectedTab === 1 ? "active" : ""}`} onClick={() => setSelectedTab(1)}>
          <Link to={{ pathname: "/wallet", }} className="navbar__list__item__link">
            <div className="navbar__list__item__link__icon">
              <FaWallet style={{ color: selectedTab === 1 ? "#f0b90b" : "#9e9fa1" }} />
            </div>
            <div className="navbar__list__item__link__title">
              Wallet
          </div>
          </Link>
        </li>
        <li className={`navbar__list__item ${selectedTab === 2 ? "active" : ""}`} onClick={() => setSelectedTab(2)}>
          <Link to={{ pathname: "/history", }} className="navbar__list__item__link">
            <div className="navbar__list__item__link__icon">
              <FaFileInvoice style={{ color: selectedTab === 2 ? "#f0b90b" : "#9e9fa1" }} />
            </div>
            <div className="navbar__list__item__link__title">
              History
          </div>
          </Link>
        </li>
      </ul>
    </div>
  );
}
