import React from "react";
import "./Header.scss";

import { FiMenu } from "react-icons/fi";

// export interface IAppProps {

// }

export default function App():JSX.Element {
  return (
    <div className="header">
      <div className="header__marker"><FiMenu size={25} /></div>
    </div>
  );
}
