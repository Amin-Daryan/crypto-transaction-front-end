import { ADD_TRANSACTION, GET_ALL_TRANSACTIONS } from "./ActionTypes";
import { Payload, ReduxState, } from "./models/models";

const initialState: ReduxState = { transactions: [] };

export interface StoreType {
    transactions?: Payload[],
}

export default (state: StoreType = initialState, action: any): StoreType => {
    const theActionType: string = action.type;
    switch (theActionType) {
        case ADD_TRANSACTION: {
            const payload: Payload = action.payload;
            if (state.transactions && state.transactions.length > 0) return { transactions: [...state.transactions, payload] };
            else return { transactions: action.payload };
        }
        case GET_ALL_TRANSACTIONS: {
            const payload: Payload[] = action.payload;
            return { transactions: payload };
        }
        default:
            return state;
    }
};