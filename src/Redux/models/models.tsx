export interface Transactions {
    type: string,
    payload: Payload[],
}

export interface Transaction {
    type: string,
    payload: Payload,
}

export interface Payload {
    id: string,
    creationDate?: any,
    payDate?: any,
    state: string,
    payedToId: string,
    fiat: {
        amount: string | number,
        currency: string
    },
    crypto: {
        amount: string | number,
        currency: string
    }
}

export interface ReduxState {
    transactions: Payload[],
}