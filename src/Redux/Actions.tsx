
import { ADD_TRANSACTION, GET_ALL_TRANSACTIONS } from "./ActionTypes";
import { Payload, Transaction, Transactions } from "./models/models";

export function getAllTransactions(payload: Payload[]): Transactions {
  return { type: GET_ALL_TRANSACTIONS, payload };
}

export function addTransaction(payload: Payload): Transaction {
  return { type: ADD_TRANSACTION, payload };
}